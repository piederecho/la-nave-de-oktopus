define([
], function () {

  function menu() {
    $('.BurgerMenu').on('click', function(){
      $('body').toggleClass('is-menuOpened');
      if($('body').hasClass('is-menuOpened')){
        $('.menuOpen').fadeIn();
      } else {
        $('.menuOpen').fadeOut();
      }
    });
  };

  return menu;
});


