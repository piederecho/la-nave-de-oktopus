require([
  'jquery',
  'menu',
  'owlslider'
  ], function ($, menu, owlslider) {

    var $doc = $(document);

		function init() {
      menu();
      loadOwl();
    }
    function loadOwl()
    {
      $('.owl-carousel').owlCarousel({
        loop:true,
        autoplay: true,
        responsive:{
          0:{
              items: 1
          }
        }
    })
    }

		$doc.ready(init);
});